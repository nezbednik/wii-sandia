# sandia-Wii
This is a fork of [sandia](https://github.com/easis/sandia) adjusted for the Wii (devkitPPC).
### Installation
With `(dkp-)pacman` installed, simply `git clone` this repository and run `makepkg -s -i`.
```
git clone https://git.donut.eu.org/libreshop/sandia-Wii.git
cd sandia-Wii && makepkg -s -i
```
### Changes from original
- Replaced every network-related library function with Wii equivalent (eg. `recv` &rarr; `net_recv`)
- Added `strip_headers`, `finish_response` options to `sandia_*_request` functions
- Added `sandia_skip_headers` function
- Removed `main.c`